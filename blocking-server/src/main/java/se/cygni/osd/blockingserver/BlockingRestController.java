package se.cygni.osd.blockingserver;

import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1.0")
public class BlockingRestController {

    @Autowired
    private GreetingService greetingService;

    @GetMapping("/greeting/{message}")
    public OutgoingResponseDto getGreeting(@PathVariable String message)
	    throws URISyntaxException, InterruptedException, ExecutionException {
	final OutgoingResponseDto response = greetingService.getGreeting(message);
	return response;
    }

}
