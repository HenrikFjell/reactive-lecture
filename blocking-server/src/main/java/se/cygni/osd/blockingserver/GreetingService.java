package se.cygni.osd.blockingserver;

import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;

public interface GreetingService {

    OutgoingResponseDto getGreeting(String message) throws URISyntaxException, InterruptedException, ExecutionException;

}