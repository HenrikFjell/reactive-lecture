package se.cygni.osd.blockingserver;

import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("!test")
public class GreetingServiceProd implements GreetingService {

    @Autowired
    private OutgoingClient client;

    @Override
    public OutgoingResponseDto getGreeting(String message)
	    throws URISyntaxException, InterruptedException, ExecutionException {
	final CompletableFuture<String> futureEcho = client.getEcho(message);
	final CompletableFuture<LocalDateTime> futureTimeStamp = client.getTimeStamp();
	CompletableFuture.allOf(futureEcho, futureTimeStamp).join();
	return createDto(futureEcho.get(), futureTimeStamp.get());
    }

    private OutgoingResponseDto createDto(String value, LocalDateTime timeStamp) {
	final OutgoingResponseDto dto = OutgoingResponseDto.builder()
		.withTimeStamp(timeStamp)
		.withValue(value)
		.build();
	return dto;
    }

}
