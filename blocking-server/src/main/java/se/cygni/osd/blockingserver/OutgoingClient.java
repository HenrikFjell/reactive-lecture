package se.cygni.osd.blockingserver;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.concurrent.CompletableFuture;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class OutgoingClient {

    @Value("${greeting.server.address}")
    private String serverAddress;

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    private RestTemplate restTemplate;

    @Async
    public CompletableFuture<String> getEcho(String message) throws URISyntaxException {
	final URI url = new URI(serverAddress + "/api/v1.0/simple/" + message);
	final String echo = restTemplate.getForObject(url, String.class);
	return CompletableFuture.completedFuture(echo);
    }

    public IncomingResponseDto getGreeting(String message) throws URISyntaxException {
	final URI url = new URI(serverAddress + "/api/v1.0/echo/" + message);
	final IncomingResponseDto greeting = restTemplate.getForObject(url, IncomingResponseDto.class);
	return greeting;
    }

    @Async
    public CompletableFuture<LocalDateTime> getTimeStamp() throws URISyntaxException {
	final URI url = new URI(serverAddress + "/api/v1.0/timestamp");
	final LocalDateTime timeStamp = restTemplate.getForObject(url, LocalDateTime.class);
	return CompletableFuture.completedFuture(timeStamp);

    }

    @PostConstruct
    private void init() {
	restTemplate = restTemplateBuilder.build();
    }

}
