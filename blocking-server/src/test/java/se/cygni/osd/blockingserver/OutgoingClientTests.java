package se.cygni.osd.blockingserver;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.ExecutionException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestClientTest(OutgoingClient.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class OutgoingClientTests {

    @Value("${greeting.server.address}")
    private String serverAddress;

    @Autowired
    private OutgoingClient client;

    @Autowired
    private MockRestServiceServer server;

    @Autowired
    private ObjectMapper objectMapper;

    private final String greetingText = "Hello";
    private final LocalDateTime timeStamp = LocalDateTime.now();

    @Test
    public void getEchoTest() throws URISyntaxException, InterruptedException, ExecutionException {
	final String greetingString = "Server got: " + greetingText;
	server.expect(requestTo(serverAddress + "/api/v1.0/simple/" + greetingText))
		.andRespond(withSuccess(greetingString, MediaType.APPLICATION_JSON));

	final String response = client.getEcho(greetingText).get();

	assertThat(response).isEqualTo(greetingString);
    }

    @Test
    public void getGreetingTest() throws URISyntaxException, JsonProcessingException {
	final IncomingResponseDto serverResponse = IncomingResponseDto.builder()
		.withTimeStamp(timeStamp)
		.withValue(greetingText)
		.build();
	final String greetingString = objectMapper.writeValueAsString(serverResponse);
	server.expect(requestTo(serverAddress + "/api/v1.0/echo/" + greetingText))
		.andRespond(withSuccess(greetingString, MediaType.APPLICATION_JSON));

	final IncomingResponseDto response = client.getGreeting(greetingText);

	assertThat(response.getValue()).isEqualTo(greetingText);
	assertThat(response.getTimeStamp()).isCloseTo(timeStamp, within(50, ChronoUnit.MILLIS));
    }

    @Test
    public void getTimeStampTest()
	    throws JsonProcessingException, URISyntaxException, InterruptedException, ExecutionException {
	final String timeStampString = objectMapper.writeValueAsString(LocalDateTime.now());
	server.expect(requestTo(serverAddress + "/api/v1.0/timestamp"))
		.andRespond(withSuccess(timeStampString, MediaType.APPLICATION_JSON));

	final LocalDateTime response = client.getTimeStamp().get();
	assertThat(response).isCloseTo(timeStamp, within(2000, ChronoUnit.MILLIS));
    }

    @BeforeAll
    public void setup() throws JsonProcessingException {
    }

}
