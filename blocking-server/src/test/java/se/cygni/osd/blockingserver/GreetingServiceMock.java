package se.cygni.osd.blockingserver;

import java.time.LocalDateTime;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("test")
public class GreetingServiceMock implements GreetingService {

    @Override
    public OutgoingResponseDto getGreeting(String message) {
	final OutgoingResponseDto response = OutgoingResponseDto.builder()
		.withTimeStamp(LocalDateTime.now())
		.withValue("Hello!")
		.build();
	return response;
    }

}
