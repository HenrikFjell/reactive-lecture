#!/bin/bash

PID=`ps -ef |grep "Threads-0.0.1-SNAPSHOT-jar-with-dependencies.jar" |grep java |awk '{print $2}'`
ROWS=`ps -M $PID |wc -l`
NUMBER_OF_THREADS=$((ROWS - 1))
echo "Number of threads is $NUMBER_OF_THREADS"
