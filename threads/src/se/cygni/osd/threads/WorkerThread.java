package se.cygni.osd.threads;

import java.util.Random;

public class WorkerThread implements Runnable {

    Random rnd = new Random();

    @Override
    @SuppressWarnings({ "squid:S0010", "squid:S2189", "squid:S2142", "squid:S00108" })
    public void run() {
	try {
	    while (true) {
		compute();
		Thread.sleep(1000);
	    }
	} catch (final InterruptedException e) {
	}
    }

    private void compute() {
	final int size = 10;
	final double[][] matrix1 = new double[size][size];

	for (int x = 0; x < size; x++) {
	    for (int y = 0; y < size; y++) {
		matrix1[x][y] = rnd.nextDouble();
	    }
	}
    }
}
