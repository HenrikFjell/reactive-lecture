export interface Greeting {

    value: string;
    timeStamp: string;
}
