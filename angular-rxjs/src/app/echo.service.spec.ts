import { TestBed } from '@angular/core/testing';

import { EchoService } from './echo.service';
import { HttpClientModule } from '@angular/common/http';

describe('EchoService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule,
    ],
  }));

  it('should be created', () => {
    const service: EchoService = TestBed.get(EchoService);
    expect(service).toBeTruthy();
  });
});
