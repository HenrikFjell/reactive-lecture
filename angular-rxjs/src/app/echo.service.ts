import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Greeting } from './greeting';

@Injectable({
  providedIn: 'root'
})
export class EchoService {

  private readonly baseUrl = 'http://localhost:8080/api/v1.0/echo/';

  constructor(private httpClient: HttpClient) { }

  getGreeting(message: string): Observable<Greeting> {
    const url = this.baseUrl + message;
    return this.httpClient.get<Greeting>(url);
  }
}
