import { Injectable } from '@angular/core';
import { KeyStrokeService } from './key-stroke.service';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  constructor(private keyStrokeService: KeyStrokeService) { }

  init() {
    this.keyStrokeService.getMessageKeys()
      .subscribe(
        {
          next(text) {
            console.log('Got typed text: ' + text);
          }
        }
      );
  }

}
