import { TestBed } from '@angular/core/testing';

import { KeyStrokeService } from './key-stroke.service';

describe('KeyStrokeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: KeyStrokeService = TestBed.get(KeyStrokeService);
    expect(service).toBeTruthy();
  });
});
