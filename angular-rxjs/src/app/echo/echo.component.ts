import { Component, OnInit } from '@angular/core';
import { EchoService } from '../echo.service';
import { KeyStrokeService } from '../key-stroke.service';

@Component({
  selector: 'app-echo',
  templateUrl: './echo.component.html',
  styleUrls: ['./echo.component.css']
})
export class EchoComponent implements OnInit {

  greetingMessage: string;

  constructor(private echoService: EchoService, private keyStrokeService: KeyStrokeService) { }

  ngOnInit() {
    const me = this;
    this.keyStrokeService.getMessageKeys()
      .subscribe(
        {
          next(text) {
            me.updateGreeting(text);
          }
        }
      );
  }

  private updateGreeting(message: string) {
    const me = this;
    this.echoService.getGreeting(message)
      .subscribe(
        {
          next(incomingGreeting) {
            me.greetingMessage = incomingGreeting.value + ' ' + incomingGreeting.timeStamp;
          },
          error(err) {
            console.error('Error in call to server: ' + err.error.error);
          }
        }
      );
  }

}
