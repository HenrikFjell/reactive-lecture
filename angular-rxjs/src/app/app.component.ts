import { Component, OnInit } from '@angular/core';
import { LoggerService } from './logger.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit {

  title = 'angular-rxjs';

  constructor(private loggerService: LoggerService) {}

  ngOnInit(): void {
    this.loggerService.init();
  }

}
