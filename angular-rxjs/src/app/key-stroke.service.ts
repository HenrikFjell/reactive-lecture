import { Injectable, OnInit } from '@angular/core';
import { fromEvent, Observable } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class KeyStrokeService {

  private messageInput: HTMLElement;

  private messageKeys: Observable<string>;

  constructor() { }

  getMessageKeys(): Observable<string> {
    if (!this.messageInput) {
      this.messageInput = document.getElementById('message-input');
      this.messageKeys = fromEvent(this.messageInput, 'input').pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length > 2),
        debounceTime(500)
      );
    }
    return this.messageKeys;
  }

}
