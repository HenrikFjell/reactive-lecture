package se.cygni.osd.echoserver;

import java.time.Duration;
import java.time.LocalDateTime;

import org.springframework.stereotype.Service;

import reactor.core.publisher.Mono;

@Service
public class EchoService {

    public Mono<String> getEcho(String request) {
	return Mono.just("Server got: " + request).delayElement(Duration.ofMillis(300));
    }

    public Mono<ResponseDto> getResponse(String request) {
	return Mono.just(createResponse(request)).delayElement(Duration.ofMillis(100));
    }

    public Mono<LocalDateTime> getTimeStamp() {
	return Mono.just(LocalDateTime.now());
    }

    private ResponseDto createResponse(String request) {
	final ResponseDto responseDto = ResponseDto.builder()
		.withTimeStamp(LocalDateTime.now())
		.withValue(request)
		.build();

	return responseDto;
    }

}
