package se.cygni.osd.echoserver;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1.0")
public class EchoRestController {

    @Autowired
    private EchoService echoService;

    @GetMapping("/echo/{greeting}")
    public Mono<ResponseDto> getEcho(@PathVariable String greeting) {
	return echoService.getResponse(greeting);
    }

    @GetMapping("/simple/{greeting}")
    public Mono<String> getSimpleEcho(@PathVariable String greeting) {
	return echoService.getEcho(greeting);
    }

    @GetMapping("/timestamp")
    public Mono<LocalDateTime> getTimeStamp() {
	return echoService.getTimeStamp();
    }

}
