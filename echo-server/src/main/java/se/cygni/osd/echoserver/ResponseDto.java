package se.cygni.osd.echoserver;

import java.time.LocalDateTime;

import javax.annotation.Generated;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(builder = ResponseDto.Builder.class)
public class ResponseDto {

    @Generated("SparkTools")
    public static final class Builder {
	private String value;
	private LocalDateTime timeStamp;

	private Builder() {
	}

	public ResponseDto build() {
	    return new ResponseDto(this);
	}

	public Builder withTimeStamp(LocalDateTime timeStamp) {
	    this.timeStamp = timeStamp;
	    return this;
	}

	public Builder withValue(String value) {
	    this.value = value;
	    return this;
	}
    }

    @Generated("SparkTools")
    public static Builder builder() {
	return new Builder();
    }

    private final String value;

    private final LocalDateTime timeStamp;

    @Generated("SparkTools")
    private ResponseDto(Builder builder) {
	value = builder.value;
	timeStamp = builder.timeStamp;
    }

    public LocalDateTime getTimeStamp() {
	return timeStamp;
    }

    public String getValue() {
	return value;
    }

}
