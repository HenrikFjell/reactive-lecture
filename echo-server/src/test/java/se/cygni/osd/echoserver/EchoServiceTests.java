package se.cygni.osd.echoserver;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

import org.assertj.core.data.Offset;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EchoServiceTests {

    private final EchoService echoService = new EchoService();
    private final String requestText = "Hello";
    private LocalDateTime testStart;

    @Test
    public void getEchoDelayTest() {
	echoService.getEcho(requestText).block();

	final long elapsedTime = getElapsedTime();
	assertThat(elapsedTime).isCloseTo(300, Offset.offset(200L));
    }

    @Test
    public void getEchoTest() {
	final String response = echoService.getEcho(requestText).block();

	assertThat(response).isEqualTo("Server got: " + requestText);
    }

    @Test
    public void getResponseDelayTest() {
	echoService.getResponse(requestText).block();

	final long elapsedTime = getElapsedTime();
	assertThat(elapsedTime).isCloseTo(100, Offset.offset(20L));
    }

    @Test
    public void getResponseTest() {
	final ResponseDto response = echoService.getResponse(requestText).block();

	assertThat(response.getValue()).isEqualTo(requestText);
	assertThat(response.getTimeStamp()).isCloseTo(testStart, within(20, ChronoUnit.MILLIS));
    }

    @Test
    public void getTimeStampTest() {
	final LocalDateTime timeStamp = echoService.getTimeStamp().block();

	assertThat(timeStamp).isCloseTo(testStart, within(20, ChronoUnit.MILLIS));
    }

    @BeforeEach
    public void init() {
	testStart = LocalDateTime.now();
    }

    private long getElapsedTime() {
	return LocalDateTime.now().toInstant(ZoneOffset.UTC).toEpochMilli()
		- testStart.toInstant(ZoneOffset.UTC).toEpochMilli();

    }
}
