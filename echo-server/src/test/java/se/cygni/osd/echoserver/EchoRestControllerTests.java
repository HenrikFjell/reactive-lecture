package se.cygni.osd.echoserver;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestInstance(value = Lifecycle.PER_CLASS)
public class EchoRestControllerTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    private final String greeting = "Hello";
    private LocalDateTime now;

    @Test
    public void echoTest() {

	final ResponseDto result = restTemplate.getForObject("http://localhost:" + port + "/api/v1.0/echo/" + greeting,
		ResponseDto.class);

	assertThat(result.getValue()).isEqualTo(greeting);
	assertThat(result.getTimeStamp()).isCloseTo(now, within(700, ChronoUnit.MILLIS));
    }

    @BeforeAll
    public void init() {
	now = LocalDateTime.now();
    }

    @Test
    public void simpleEchoTest() {
	final String result = restTemplate.getForObject("http://localhost:" + port + "/api/v1.0/simple/" + greeting,
		String.class);

	assertThat(result).isEqualTo("Server got: " + greeting);
    }

    @Test
    public void timeStampTest() {
	final LocalDateTime result = restTemplate.getForObject("http://localhost:" + port + "/api/v1.0/timestamp/",
		LocalDateTime.class);

	assertThat(result).isCloseTo(now, within(700, ChronoUnit.MILLIS));
    }

}
