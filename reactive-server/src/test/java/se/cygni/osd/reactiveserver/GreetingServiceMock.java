package se.cygni.osd.reactiveserver;

import java.time.LocalDateTime;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import reactor.core.publisher.Mono;

@Service
@Profile("test")
public class GreetingServiceMock implements GreetingService {

    @Override
    public Mono<OutgoingResponseDto> getGreeting(String message) {
	final OutgoingResponseDto responseDto = OutgoingResponseDto.builder()
		.withTimeStamp(LocalDateTime.now())
		.withValue("Hello!")
		.build();
	return Mono.just(responseDto);
    }

}
