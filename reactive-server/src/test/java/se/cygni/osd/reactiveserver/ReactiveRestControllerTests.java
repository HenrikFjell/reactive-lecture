package se.cygni.osd.reactiveserver;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = { "test" })
public class ReactiveRestControllerTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getGreetingTest() {
	final LocalDateTime now = LocalDateTime.now();

	final OutgoingResponseTestDto result = restTemplate.getForObject(
		"http://localhost:" + port + "/api/v1.0//greeting/Hello",
		OutgoingResponseTestDto.class);

	assertThat(result.getValue()).isEqualTo("Hello!");
	assertThat(result.getTimeStamp()).isCloseTo(now, within(500, ChronoUnit.MILLIS));
    }

}
