package se.cygni.osd.reactiveserver;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import reactor.core.publisher.Mono;

@Service
@Profile("!test")
public class GreetingServiceProd implements GreetingService {

    @Autowired
    private OutgoingClient client;

    @Override
    public Mono<OutgoingResponseDto> getGreeting(String message) {
	final Mono<String> echoMessageMono = client.getSimpleEcho(message);
	final Mono<LocalDateTime> timeStampMono = client.getTimeStamp();
	return echoMessageMono
		.flatMap(echoMessage -> timeStampMono.map(timeStamp -> createDto(echoMessage, timeStamp)));
    }

    private OutgoingResponseDto createDto(String value, LocalDateTime timeStamp) {
	final OutgoingResponseDto dto = OutgoingResponseDto.builder()
		.withTimeStamp(timeStamp)
		.withValue(value)
		.build();
	return dto;
    }
}
