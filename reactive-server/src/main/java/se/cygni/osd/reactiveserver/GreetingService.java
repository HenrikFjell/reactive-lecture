package se.cygni.osd.reactiveserver;

import reactor.core.publisher.Mono;

public interface GreetingService {

    Mono<OutgoingResponseDto> getGreeting(String message);

}