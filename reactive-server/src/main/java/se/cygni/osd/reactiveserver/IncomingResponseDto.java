package se.cygni.osd.reactiveserver;

import java.time.LocalDateTime;

import javax.annotation.Generated;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(builder = IncomingResponseDto.Builder.class)
public class IncomingResponseDto {

    @Generated("SparkTools")
    public static final class Builder {
	private String value;
	private LocalDateTime timeStamp;

	private Builder() {
	}

	public IncomingResponseDto build() {
	    return new IncomingResponseDto(this);
	}

	public Builder withTimeStamp(LocalDateTime timeStamp) {
	    this.timeStamp = timeStamp;
	    return this;
	}

	public Builder withValue(String value) {
	    this.value = value;
	    return this;
	}
    }

    @Generated("SparkTools")
    public static Builder builder() {
	return new Builder();
    }

    private final String value;

    private final LocalDateTime timeStamp;

    @Generated("SparkTools")
    private IncomingResponseDto(Builder builder) {
	value = builder.value;
	timeStamp = builder.timeStamp;
    }

    public LocalDateTime getTimeStamp() {
	return timeStamp;
    }

    public String getValue() {
	return value;
    }

}
