package se.cygni.osd.reactiveserver;

import java.time.LocalDateTime;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;

@Component
public class OutgoingClient {

    @Value("${greeting.server.address}")
    private String serverAddress;

    @Autowired
    private WebClient.Builder webClientBuilder;

    private WebClient webClient;

    public Mono<IncomingResponseDto> getGreeting(String message) {
	final Mono<IncomingResponseDto> greeting = webClient
		.get()
		.uri("/api/v1.0/echo/{message}", message)
		.retrieve()
		.bodyToMono(IncomingResponseDto.class);
	return greeting;
    }

    public Mono<String> getSimpleEcho(String message) {
	return webClient
		.get()
		.uri("/api/v1.0/simple/{message}", message)
		.retrieve()
		.bodyToMono(String.class);
    }

    public Mono<LocalDateTime> getTimeStamp() {
	return webClient
		.get()
		.uri("/api/v1.0/timestamp")
		.retrieve()
		.bodyToMono(LocalDateTime.class);
    }

    @PostConstruct
    private void init() {
	webClient = webClientBuilder.baseUrl(serverAddress).build();
    }

}
