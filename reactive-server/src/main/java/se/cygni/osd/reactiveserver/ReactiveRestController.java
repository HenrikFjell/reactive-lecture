package se.cygni.osd.reactiveserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1.0")
public class ReactiveRestController {

    @Autowired
    private GreetingService greetingService;

    @GetMapping("/greeting/{message}")
    public Mono<OutgoingResponseDto> getGreeting(@PathVariable String message) {
	final Mono<OutgoingResponseDto> response = greetingService.getGreeting(message);
	return response;
    }

}
