#!/bin/bash

# This script starts the application with port 9999 enabled for JConsole
# Note that java.rmi.server.hostname must be changed to actual value

java -Dcom.sun.management.jmxremote.port=9999 \
        -Dcom.sun.management.jmxremote.authenticate=false \
        -Dcom.sun.management.jmxremote.ssl=false \
        -Dcom.sun.management.jmxremote=true \
        -Dcom.sun.management.jmxremote.local.only=false \
        -Djava.rmi.server.hostname=192.168.56.104 \
        -jar reactive-server-1.0.0-SNAPSHOT.jar 
